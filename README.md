# Lesson 1.3


# Описание задания
```sh
Необходимо собрать nginx из исходников со стандартными модулями и запустить 
бинарник во втором образе
Образы Debian9
При запуске примаунтить конфиг в контейнер.
Задача для прохождения на 100% (выполнять не обязательно)
Добавить в сборку
https://github.com/openresty/lua-nginx-module

```


## 1. Description Docker-файла:
Докер файл состоит из двух "логических" частей. В первой части, компилируется NGINX+LUA из исходников. Во второй части создается новая программная область с бинарным файлом из первой части Docker-файла. Данная сборка называется многоэтапная(MultiStage).
## 2. Description libraries and Utilities using in DockerFile  
##### Wget - Утилита для скачивания удаленных файлов по указанию URL.
##### gcc - Компилятор для языка программирования С.
##### make - Cборщик файлов, преобразует один тип данных в другой.
##### pcre - Библиотека, реализующая работу регулярных выражений в стиле Perl.
##### zlib1g - Библиотека сжатия данных.
##### libssl - Библиотека реализующая ssl-соединение.


## 3. Description install first path
### Скачать исходники Nginx + Lua:

```sh
LuaJIT-2.0.4.tar.gz
nginx-1.6.0.tar.gz
nginx_devel_kit.tar.gz
nginx_lua_module.tar.gz
```
#### Примечание:
Lua не устанавливалось с последней версией nginx_1.13.*  поэтому в данном проект используется nginx версии 1.6.0 именно с данной версии, начинается поддержка lua. 

### Распаковка архивов .tar и конфигурирование
1. Для распаковки каждого архива применяется команда ``` tar xvf <name_archive>``` .
2. Переход в распокованный каталог LuaJIT-2.0.4 и выполнение команды ```make install```.
3. Переход в распокованный каталог сервера nginx и выполнение команды ``` LUAJIT_LIB=/usr/local/lib LUAJIT_INC=/usr/local/include/luajit-2.0```(Данная команда устанавливает переменные среды)
4. Вызов команды:
```sh
./configurature --with-cc-opt=-O2 --with-ld-opt='-Wl,-rpath,/usr/local/lib' \
--add-module=/tmp/nginx/ngx_devel_kit-0.3.0 \
--add-module=/tmp/nginx/lua-nginx-module-0.10.8 
```
5. Выполнение команды ```make install ``` (собирает BIN-файл nginx)

## 4. Description install second path:
```sh
FROM debian:9
WORKDIR /usr/local/nginx/sbin/
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
COPY --from=build /usr/lib/x86_64-linux-gnu/libcrypto.so.1.1 /usr/lib/
COPY --from=build /usr/local/lib/libluajit-5.1.so.2 /usr/local/lib
CMD ["./nginx","-g","daemon off;"]
```
- Команда ```FROM debian:9``` говорит о том, что контейнер будет собиратья на базе данной ОС.
-  ``` WORKDIR /usr/local/nginx/sbin/ ``` устанавливает рабочую дирректорию.
-  ``` COPY --from=build  /path``` копирует из первой части "build" во вторую необходимые библиотеки для запуска.
-  ``` RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx ``` создает директории необходимые для работы nginx.
- ``` CMD ["./nginx","-g","daemon off;"] ``` Останавливает процесс 

## 5. Building and check of work
- Сборка докер контейнера производится следующей командой:
```docker build -t <name> -f path . (где path-путь расположения файла, <name> - докер контейнера)```
- Запуск докер контейнера производится следующей командой 
```docker run -d -ti -p number_port:80 -v /path/nginx.conf:/usr/local/nginx/conf/nginx.conf <image_id> (numebr_port-номер порта соединения, path путь до файла)```
 Предварительно на хостовой машине были внесены изменения в файл ```nginx.conf``` и добавлена область для проверки работы LUA.

 ##### Проверка работы Nginx и Lua:
- Выполнить команду ```curl "http://127.0.0.1:number_port" ```, если все настроено корректно комнада выведет ошибку "404"
- Выполнить команду ``` curl "http://localhost/sum/?a=10&b=20" ```, eсли все настроено корректно команда выведет "30". 






